﻿using System;

namespace Cocus.Exercise2
{
    class Program
    {
        static void Main(string[] args)
        {
            var messageA = new MessageA();
            var messageB = new MessageB();
            var messageC = new MessageC();

            VerifyTypeClass(messageA);
            VerifyTypeClass(messageB);
            VerifyTypeClass(messageC);

            Console.ReadKey();
        }

        public static void VerifyTypeClass(Message message)
        {
            message.MyCustomMethod();

            if(message is MessageB)
            {
                var messageB = message as MessageB;
                messageB.SomeAdditionalMethodB();
            }
        }
    }
}
