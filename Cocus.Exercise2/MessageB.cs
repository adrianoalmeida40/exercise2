﻿using System;

namespace Cocus.Exercise2
{
    public class MessageB : Message
    {
        public override void MyCustomMethod()
        {
            Console.WriteLine("MyCustomMethod(): Message Type B\n");
        }

        public void SomeAdditionalMethodB()
        {
            Console.WriteLine("SomeAdditionalMethodB(): Message Type B\n");
        }
    }
}
