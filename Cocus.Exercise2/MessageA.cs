﻿using System;

namespace Cocus.Exercise2
{
    public class MessageA : Message
    {
        public override void MyCustomMethod()
        {
            Console.WriteLine("MyCustomMethod(): Message Type A\n");
        }
    }
}
