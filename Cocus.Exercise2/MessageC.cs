﻿using System;

namespace Cocus.Exercise2
{
    public class MessageC : Message
    {
        public override void MyCustomMethod()
        {
            Console.WriteLine("MyCustomMethod(): Message Type C\n");
        }
    }
}
